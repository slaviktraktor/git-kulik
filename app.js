var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require( 'cors');

var index = require('./routes/index');
var db = require('./dbFuncs');

var app = express();

const mongoose = db.setUpConnection();

app.use(cors({origin: '*'}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', index);

module.exports = app;
