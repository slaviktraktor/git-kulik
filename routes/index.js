var express = require('express');
var db = require('../dbFuncs');
var router = express.Router();


router.get('/:id', (req, res) => {
  db.getData(req.params.id)
    .then(data => res.send(data))
    .catch(err => console.log("Get error", err));
});

router.post('/', (req, res) => {
  db.createData(req.body.id, req.body.data)
    .then(() => res.send({
      res: true
    }))
    .catch(err => console.log("Create error", err));
})

router.delete('/', (req, res) => {
  db.deleteData(req.body.id)
    .then(data => res.send({
      res: true
    }))
    .catch(err => console.log("Delete error", err));
})

module.exports = router;
