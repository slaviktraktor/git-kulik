var mongoose  = require( "mongoose");

const DataSchema = new mongoose.Schema({
	data :{ type: String },
	id :{type: Number, required: true }
});

mongoose.model('data', DataSchema);

const Data = mongoose.model('data');

function setUpConnection() {
    mongoose.Promise = global.Promise;
    mongoose
        .connect(`mongodb://localhost:27017/kulik`, { useNewUrlParser: true })
        .then(console.log("db connect is OK"))
        .catch(e => console.log(e.name + ': ' + e.message));
    return mongoose;
}

async function getData(id) {
    return Data
        .find({id: id})
        .select({_id: 0, id: 1, data: 1});
}

async function createData(id, data) {
    const note = new Data({
        id: id,
        data: data,
    });
    
    return note.save();
}

async function deleteData(id) {
    return Data.deleteOne({id: id});
}

module.exports = {
    setUpConnection,
    createData,
    getData,
    deleteData
}